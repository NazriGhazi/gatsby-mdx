exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions
  const {
    data: {
      allMdx: { edges: posts },
    },
  } = await graphql(`
    {
      allMdx {
        edges {
          node {
            frontmatter {
              slug
            }
          }
        }
      }
    }
  `)

  posts.forEach(({ node }) => {
    const { slug } = node.frontmatter
    createPage({
      path: slug, //url
      component: require.resolve("./src/templates/post-template.js"), //template
      context: {
        slug: slug, //variable
      },
    })
  })
}
